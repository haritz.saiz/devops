# Kubernetes - Instalacion Manual (over Docker)

## Table of contents

1. [Requisitos](#requisitos)
2. [En Master y todos los Workers](#master-and-worker)
    - [Desactivar memoria SWAP](#swap-disable)
    - [Hostname único](#hostname)
    - [Conexion entre hosts](#hosts)
    - [Instalar OpenSSH Server](#openssh)
    - [Instalar CRI (Container Runtime Interface): Docker ](#cri)
    - [Instalar kubeadam, kubelet y kubect](#kubeadm-kubectl-kubelet)
    - [Iniciar Kubernetes](#init)
    - [Cambiar DNS de CoreOS a Flannel](#dns)
3. [Unir workers al cluster](#join)
3. [Troubleshooting](#tr)
    - [Kubernetes cluster remove](#remove)


<a name="requisitos"/>

## Requisitos

Se necesita almenos un nodo master y otro nodo worker con las siguientes características: 

| Master         | Worker        |
|----------------|---------------|
| 2 GB RAM       | 1 GB RAM      |
| 2 Cores of CPU | 1 Core of CPU |

Esta guia configurará un cluster con **1 nodo master y 2 worker**. Esta guia utilizará los siguientes hostnames:

`node1`: master (api-server, controller, etcd, scheduler, dns)

`node2` y `node3`: workers

<a name="master-and-worker"/>

## En Master y todos los Workers

Los primeros comandos a ejecutar requieren ser _superuser_. Algunos comandos no funcionan con sudo y deben se rlanzados por el usuario _root_

```
sudo su
```

Empecemos actualizando los repositiorios de linux:

```
apt-get update
```

<a name="swap-disable"/>

### Desactivar memoria SWAP

Luego debemos desactivar la memoria swap de los hosts:

```
swapoff -a
```
De la misma manera debemos comentar el volumen o _mounting point_ SWAP del fichero `/etc/fstab`:

![](k8s/imgs/fstab.PNG)


<a name="hostname"/>

### Hostname único

Kubernetes neceista que cada nodo se llame de distinta manera. En este caso, es necesario modificar el archivo `/etc/hostname`. 


<a name="hosts"/>

### Conexion entre hosts

En caso de utilizar maquinas virtuales, es recomendable asegurarse que las direcciones MAC de los adaptadores de red son distintas para todas las máquinas/nodos:

![](k8s/imgs/fstab.PNG)

De la misma manera que las dierecciones MAC, cada host debe tener direcciones IPs distintas. Es recomendable utilizar IPs estáticas. Modifica el fichero `/etc/network/interfaces`

```
ifup enp0s3
ifdown enp0s3
```

![](k8s/imgs/network-interfaces.PNG)

Tras configurar todas las IPs de forma estática, tenemos los siguientes nodos con sus respectivas direcciones. 

|          |         |
|----------|---------|
| node1    | 192.168.1.110  |
| node2    | 192.168.1.111  |
| node3    | 192.168.1.112  |

Es recomendable comprobar que hay conexion entre los nodos (i.e. `ping`)

Para simplificar la comunicacion entre los nodos vamos a modificar los ficheros `/etc/hosts` de todos los nodos para que se comuniquen con los hostnames asignados:

![](k8s/imgs/hosts.PNG)


<a name="openssh"/>

### Instalar OpenSSH Server 

```
sudo apt-get install openssh-server  


```
<a name="cri"/>

### Instalar CRI (Container Runtime Interface): Docker 

Tanto el master node como los worker, requieren de un entorno de ejecucion de contenedores.

```
apt-get update 

apt-get install -y docker.io

docker --version

```

o

``` 
wget -qO- https://get.docker.com/ | sh

sudo usermod -aG docker <usuario>

docker --version
```

<a name="kubeadm-kubectl-kubelet"/>

### Instalar kubeadam, kubelet y kubectl

Descargar Curl
```
apt-get install -y apt-transport-https ca-certificates curl
```

Descargar la clave pública de Google-Cloud
```
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
```

Añadir el repo de Kuberentes:

```
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
```

Actualizar los paquetes de nuevo:

```
apt-get update
```

Instalar los 3 paquetes principales 🚀
```
apt-get install -y kubelet kubeadm kubectl
```


<a name="init"/>

### Iniciar Kubernetes

**Nota: Al utlizar el CRI (Container runtime Interface) de Docker, kubernetes detectará automaticamente el cgroup a utilizar. Para más info: https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#configure-cgroup-driver-used-by-kubelet-on-control-plane-node**


Antes de iniciar el cluster de Kubernetes debemos seleccionar el Container Network Interface provider or CNI.


https://rancher.com/blog/2019/2019-03-21-comparing-kubernetes-cni-providers-flannel-calico-canal-and-weave/

kubeadm init --apiserver-advertise-address=<ip-address-of-kmaster-vm> --pod-network-cidr=<cni-cidr-range>

El _cni-cidr-range_ puede tomar alguno de estos valores:

|          | cni-cidr-range |
|----------|---------|
| Flannel  | 10.244.0.0/16  |
| Calico   | 192.168.0.0/16 |

En este caso utilizaremos el CNI de Flannel y el nodo master será el nodo1 (cuya IP es 192.168.1.110):

**Nota: el comando kubeadm init SOLO se ejecuta en el MASTER NODE**

```
kubeadm init --apiserver-advertise-address=192.168.1.110 --pod-network-cidr=10.244.0.0/16 
``` 

El comando init creará diversos ficheros de configuracion, certificados y desplegara los contenedores docker con los distintos elementos de kubernetes: Api-Server, scheduler, etc.

![](k8s/imgs/kubeadm-init.PNG)

Una vez terminado el proceso de inicializacion del cluster, debemos ejecutar los siguientes comandos como **usario NO root**

```
mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

<a name="dns"/>

### Cambiar DNS de CoreOS a Flannel

Sesplegamos el DNS de Flannel:

```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

![](k8s/imgs/kube-flannel.PNG)

<a name="join"/>

## Unir workers al cluster

Finalmente, tras haber ejecutado los mismos pasos en todos los worker nodes, ejecutamos el siguiente comando para unir un nuevo nodo al cluster de kubernetes como **root user**:

```
kubeadm join 192.168.1.110:6443 --token adgw4u.jbfg5vwkbuzzhqji \
    --discovery-token-ca-cert-hash sha256:45405cf9205a25fc14dfd605ac2ae235aa689bbbce1a46e85fdbf38eff938a40
```
Desde el control-plane node aka. Master node, podemos observar que nuevos nodos se han unido al cluster:

![](k8s/imgs/kube-nodes-join.PNG)

Sin embargo, los nodos no tienen un rol asignado aun. Para ello, ejecutamos lo siguiente:

```
kubectl label node node2 node-role.kubernetes.io/worker=worker

kubectl label node node3 node-role.kubernetes.io/worker=worker

```
![](k8s/imgs/kube-node-worker-label.PNG)


<a name="tr"/>

## Troubleshooting

<a name="remove"/>

### Kubernetes cluster remove 

Para borrar el cluster de kubernetes es necesario disponer de acceso a la cuenta __root__. Desde ese usuario se ejecuta el siguiente comando:

```
kubeadm reset
```
