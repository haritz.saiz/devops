---
- hosts: master
  become: true
  
  handlers:
  - name: docker status
    service: name=docker state=started

  tasks:
  - name: Install packages that allow apt to be used over HTTPS (1/26)
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg-agent
      - software-properties-common

  - name: Add an apt signing key for Docker (2/26)
    apt_key:
      url: https://download.docker.com/linux/ubuntu/gpg
      state: present

  - name: Add apt repository for stable version (3/26)
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
      state: present

  - name: Install docker and its dependecies (4/26)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - docker-ce 
      - docker-ce-cli 
      - containerd.io
    notify:
      - docker status

  - name: Add {{ user }} user to docker group (5/26)
    user:
      name: "{{ user }}"
      group: docker
  
  - name: Disabling swap - Disable swap (6/26)
    command: swapoff -a
    when: ansible_swaptotal_mb > 0

  - name: Disabling swap - Remove swapfile from /etc/fstab (7/26)
    mount:
      name: "{{ item }}"
      fstype: swap  
      state: absent
    with_items:
      - swap
      - none

  - name: Add an apt signing key for Kubernetes (8/26)
    apt_key:
      url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
      state: present

  - name: Adding apt repository for Kubernetes (9/26)
    apt_repository:
      repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
      state: present
      filename: kubernetes.list

  - name: Installing Kubernetes binaries (10/26)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
        - kubelet 
        - kubeadm 
        - kubectl

  - name: Restart kubelet (12/26)
    service:
      name: kubelet
      daemon_reload: yes
      state: restarted

  - name: Initialize the Kubernetes cluster using kubeadm (13/26)
    command: kubeadm init --apiserver-advertise-address="{{ node_ip }}" --apiserver-cert-extra-sans="{{ node_ip }}" --pod-network-cidr=10.0.0.0/16

  - name: Setup kubeconfig for {{ user }} user (14/26)
    command: "{{ item }}"
    with_items:
     - mkdir -p /home/{{ user }}/.kube
     - cp /etc/kubernetes/admin.conf /home/{{ user }}/.kube/config
     - chown {{ user }}:{{ user }} /home/{{ user }}/.kube/config
    
  - name: Install Calico pod network (16/26)
    become: false
    command: kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
  
  - name: Generate join command (17/26)
    command: kubeadm token create --print-join-command
    register: join_command

  - name: Copy join command to local file (18/26)
    become: false
    local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"

  - name: Install pip3. Required by Ansible plugin - community.kubernetes (19/26)
    apt: 
      name: "python3-pip"
      state: present
      update_cache: yes
    
  - name: Install openshift. Required by Ansible plugin - community.kubernetes (20/26)
    pip:
      name: openshift

  - name: Deploy Kubernetes Dashboard 2.0.0 (21/26)
    become: false
    command: kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml

  - name: Modify Kubernetes Dashboard 2.0.0 Service to NodePort (22/26)
    become: false
    community.kubernetes.k8s_service:
      state: present
      name: kubernetes-dashboard
      namespace: kubernetes-dashboard
      type: NodePort
      ports:
      - port: 443
        targetPort: 8443
  
  - name: Add Admin role to kubernetes-dashboard ServiceAccount (23/26)
    become: false
    shell: | 
      cat <<EOF | kubectl apply -f -
      apiVersion: rbac.authorization.k8s.io/v1
      kind: ClusterRoleBinding
      metadata:
        name: admin-user
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: cluster-admin
      subjects:
      - kind: ServiceAccount
        name: kubernetes-dashboard
        namespace: kubernetes-dashboard
      EOF
  
  - name: Get Kubernetes Dashboard Service Account token (24/26)
    become: false
    shell: kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa kubernetes-dashboard -o jsonpath="{.secrets[0].name}") -o go-template="{{ '{{' }}.data.token | base64decode{{ '}}' }}"
    register: service_account_token
  
  - name: Get Kubernetes Dashboard Service NodePort (25/26)
    become: false
    command: kubectl get svc kubernetes-dashboard -n kubernetes-dashboard -o jsonpath="{.spec.ports[0].nodePort}"
    register: service_port
  
  - name: iptables port forwarding (26/26)
    command: iptables -A FORWARD -j ACCEPT

  - debug: msg="{{ item.prop }} - {{ item.value }}"
    with_items: 
    - { prop: 'K8s Dashboard PORT', value: "{{ service_port.stdout }}" }
    - { prop: 'Service Account Token', value: "{{ service_account_token.stdout }}" }
