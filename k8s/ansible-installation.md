# Kubernetes - Instalacion con Ansible

## Table of contents

1. [Requisitos](#requisitos)
2. [Resources](#requisitos)

## Resources

### Master Playbook

```
---
- hosts: master
  become: true
  
  handlers:
  - name: docker status
    service: name=docker state=started

  tasks:
  - name: Install packages that allow apt to be used over HTTPS (1/18)
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg-agent
      - software-properties-common

  - name: Add an apt signing key for Docker (2/18)
    apt_key:
      url: https://download.docker.com/linux/ubuntu/gpg
      state: present

  - name: Add apt repository for stable version (3/18)
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
      state: present

  - name: Install docker and its dependecies (4/18)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - docker-ce 
      - docker-ce-cli 
      - containerd.io
    notify:
      - docker status

  - name: Add {{ user }} user to docker group (5/18)
    user:
      name: "{{ user }}"
      group: docker
  
  - name: Disabling swap - Disable swap (6/18)
    command: swapoff -a
    when: ansible_swaptotal_mb > 0

  - name: Disabling swap - Remove swapfile from /etc/fstab (7/18)
    mount:
      name: "{{ item }}"
      fstype: swap  
      state: absent
    with_items:
      - swap
      - none

  - name: Add an apt signing key for Kubernetes (8/18)
    apt_key:
      url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
      state: present

  - name: Adding apt repository for Kubernetes (9/18)
    apt_repository:
      repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
      state: present
      filename: kubernetes.list

  - name: Installing Kubernetes binaries (10/18)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
        - kubelet 
        - kubeadm 
        - kubectl

  - name: Restart kubelet (12/18)
    service:
      name: kubelet
      daemon_reload: yes
      state: restarted

  - name: Initialize the Kubernetes cluster using kubeadm (13/18)
    command: kubeadm init --apiserver-advertise-address="{{ node_ip }}" --apiserver-cert-extra-sans="{{ node_ip }}" --pod-network-cidr=10.244.0.0/16 

  - name: Setup kubeconfig for {{ user }} user (14/18)
    command: "{{ item }}"
    with_items:
     - mkdir -p /home/{{ user }}/.kube
     - cp /etc/kubernetes/admin.conf /home/{{ user }}/.kube/config
     - chown {{ user }}:{{ user }} /home/{{ user }}/.kube/config
  
  - name: Delete CoreDNS (15/18)
    become: false
    command: kubectl delete --namespace=kube-system deployment coredns
  
  - name: Install Flannel pod network (16/18)
    become: false
    command: kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
  
  - name: Generate join command (17/18)
    command: kubeadm token create --print-join-command
    register: join_command

  - name: Copy join command to local file (18/18)
    become: false
    local_action: copy content="{{ join_command.stdout_lines[0] }}" dest="./join-command"
```

```
ansible-playbook -i inventory install-k8s-master.playbook.yml --extra-vars "node_ip=172.31.16.10 user=ubuntu"
```

### Worker Playbook

```
---
- hosts: workers
  become: true

  handlers:
  - name: docker status
    service: name=docker state=started

  tasks:
  - name: Install packages that allow apt to be used over HTTPS (1/14)
    apt:
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - apt-transport-https
      - ca-certificates
      - curl
      - gnupg-agent
      - software-properties-common

  - name: Add an apt signing key for Docker (2/14)
    apt_key:
      url: https://download.docker.com/linux/ubuntu/gpg
      state: present

  - name: Add apt repository for stable version (3/14)
    apt_repository:
      repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
      state: present

  - name: Install docker and its dependecies (4/14)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
      - docker-ce 
      - docker-ce-cli 
      - containerd.io
    notify:
      - docker status

  - name: Add 'vagrant' user to docker group (5/14)
    user:
      name: vagrant
      group: docker
  
  - name: Disabling swap - Disable swap (6/14)
    command: swapoff -a
    when: ansible_swaptotal_mb > 0

  - name: Disabling swap - Remove swapfile from /etc/fstab (7/14)
    mount:
      name: "{{ item }}"
      fstype: swap  
      state: absent
    with_items:
      - swap
      - none

  - name: Add an apt signing key for Kubernetes (8/14)
    apt_key:
      url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
      state: present

  - name: Adding apt repository for Kubernetes (9/14)
    apt_repository:
      repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
      state: present
      filename: kubernetes.list

  - name: Installing Kubernetes binaries (10/14)
    apt: 
      name: "{{ packages }}"
      state: present
      update_cache: yes
    vars:
      packages:
        - kubelet 
        - kubeadm 
        - kubectl

  - name: Restart kubelet (12/14)
    service:
      name: kubelet
      daemon_reload: yes
      state: restarted

  - name: Copy the join command to server location (13/14)
    copy: 
      src: join-command 
      dest: /tmp/join-command.sh 
      mode: 0777

  - name: Join the node to cluster (14/14)
    command: sh /tmp/join-command.sh
```

```
ansible-playbook -i inventory install-k8s-worker.playbook.yml
```