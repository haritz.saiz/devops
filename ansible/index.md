# Ansible

## Table of contents

1. [Conceptos de Ansible](#overview)
2. [Instalacion de Ansible](#setup)
    - [Ubuntu](#j)
    - [Debian](#setup-debian)
3. [Usando Ansible](#usage)
    - [Capitulo 1: Conceptos básicos y primeros pasos con Ansible y Vagrant](#cap1)
        - [Vagrant](#vagrant)
        - [Modules: (i.e. Ping / Pong)](#usage-modules)
        - [Commands: (i.e. "df-h")](#usage-cmd)
        - [Playbook básico](#basic-playbook)
        - [Vagrant provision with playbook (Linux/Mac)](#playbook-vagrant)
        - [ansible.cfg](#ansible-cfg)
    - [Capitulo 2: Inventarios](#cap2)
    - [Capitulo 3: Playbooks](#cap3)
    - [Capitulo 4: Playbooks avanzado: Variables, Env Vars y handlers](#cap4)
    - [Capitulo 5: Inventarios Avanzados](#cap5)


<a name="overview"/>

## Conceptos de Ansible

![](ansible/imgs/architecture.PNG)


<a name="setup"/>

## Instalacion de Ansible


### Ubuntu (TBD and TBT)

Guia de referencia: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu



<a name="setup-debian"/>

### Debian

Guia de referencia: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-debian

Añadir la siguiente linea en el fichero `/etc/apt/sources.list`

```
deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
```

Luego ejecutar los siguientes comandos:

```
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367

sudo apt update

sudo apt install ansible
```

<a name="setup-debian"/>

## Usando Ansible

<a name="cap1"/>

# Capitulo 1: Conceptos básicos y primeros pasos con Ansible y Vagrant

Vamos a suponer que tenemos 3 tipos de hosts remotos a configurar. Uno de ellos será de tipo "database" y otras dos de tipo "webserver". Para ello modificamos el contenido del fichero 

`/etc/ansible/hosts` indicando las IPs para cada tipo de servidor remoto:

```
[database]
192.168.1.110

[webserver]
192.168.1.111
192.168.1.112
```

o podriamos llamarles de otra manera: 
```
[master]
192.168.1.110

[workers]
192.168.1.111
192.168.1.112
```

Nos quedaremos con esta última para el resto de pruebas.

Tambien podemos crear un fichero de inventario en cualquier sitio. A la hora de ejecutar los comandos de Ansible, usaremos la opcion `-i <path-al-fichero-inventario> <grupo>`. Por ejemplo, supongamos que tenemos el mismo inventario definido anteriormente en el directorio `/home/debian/inventory/k8s-inventory`. La ejecucion de ansible seria: `-i /home/debian/inventory/k8s-inventory worker` 

**Nota IMPORTANTE: Los host especificados en el inventario deberán tener alguna version de python instalada para que ansible funcione**

**Nota Recomendable: es importante que los hosts o servidores remotes son accesibles mediante claves SSH (En caso de no estar configurados, copia la clave publica en cada host en el fichero .ssh/authorized_keys). Si se prefiere utilizar la contraseña y no una clave priv/pub, se puede añadir el flag `--ask-pass` or `-k` al comando de ansible para escribir la contraseña ssh. De requerir permisos de superusuario/root añadir el flag `-b` (-b de become. "become superuser" o "become root") , y si para acceder a dicho usuario se necesita una contraseña (depnde de la configuración, puede que se necesite),  añadir el flag `--ask-become-pass` o `-K`**


Vamos a ver la lista de hosts configurados:

```
ansible-inventory --list -y
```
![](ansible/imgs/inventory.PNG)

<a name="vagrant"/>

#### Vagrant

Vagrant nos permite aprovisionar máquinas virtuales de forma rápida y sencilla, perfecto para un entorno de desarrollo con una o varias máquinas.

**Requisitos**: Necesitaremos instalar Vagrant y Virtual Box:
- https://www.vagrantup.com/downloads
- https://www.virtualbox.org/wiki/Downloads

Por último, una vez descargado Vagrant, nos descargaremos un plugin que nos permitirá configurar el tamaño del disco de cada VM:

```
vagrant plugin install vagrant-disksize
```

Empezaremos inicializando el entorno de vagrant:

```
vagrant init
```
Este comando creará un fichero llamado `Vagrantfile` (escrito con la sintaxsis de rubi) en el directorio donde se ejecute el comando. 

Empezaremos seleccionando el sistema operativo. Para ello modificaremos el campo `config.vm.box` En la url https://vagrantcloud.com/search podemos encontrar distintas alternativas, por ejemplo un ubuntu 16.04 tambien conocido como ubuntu xenial. Configuraremos el campo anterior de la siguiente manera:
``` 
config.vm.box = "generic/ubuntu2010"
``` 

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu2010"
  end
end
```

Pero ... en este caso queremos aprovisionar no una mquina sino 3!!. Por ello vamos a modificar el archivo Vagrantfile para indicar nuevas máquinas. Es importante no confundirse con la propiedad `Vagrant.configure("2")` pues esta propiedad identifica la version del script, parecido que en los docker-compose:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define "node1" do |web|
    config.vm.box = "generic/ubuntu2010"
  end

  config.vm.define "node2" do |db|
    config.vm.box = "generic/ubuntu2010"
  end

  config.vm.define "node3" do |db|
    config.vm.box = "generic/ubuntu2010"
  end
end
```

Y si quisieramos 5 máquinas ?? Y si ademas queremos especificar el hostname, IP estatica, uso de CPU y memoria de cada máquina y disco ?? Sin problemas, podemos modificar el Vagrantfile para hacerlo algo más dinámico:

- IP estática/dinámica: https://www.vagrantup.com/docs/networking/private_network 
- Set CPU and mem: https://www.vagrantup.com/docs/providers/virtualbox/configuration#vboxmanage-customizations
- Set disk: https://stackoverflow.com/questions/49822594/vagrant-how-to-specify-the-disk-size

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

cluster = {
  "master" => { :ip => "192.168.10.100", :cpus => 2, :mem => 1024, :disk=> "10GB" },
  "worker-1" => { :ip => "192.168.10.101", :cpus => 1, :mem => 1024, :disk=> "10GB"},
  "worker-2" => { :ip => "192.168.10.102", :cpus => 1, :mem => 1024, :disk=> "10GB"}
}
Vagrant.configure("2") do |config|

  cluster.each_with_index do |(hostname, info), index|

    config.vm.define hostname do |cfg|
      cfg.vm.provider :virtualbox do |vb, override|
        config.vm.box = "ubuntu/trusty64"
        config.disksize.size = info[:disk]
        override.vm.network :private_network, ip: "#{info[:ip]}", virtualbox__intnet: true
        override.vm.hostname = hostname
        vb.name = hostname  
        vb.memory = info[:mem]
        vb.cpus = info[:cpus]
      end # end provider
    end # end config

  end # end cluster
end
```
Una vez definidas las máquinas, las creamos:

```
vagrant up
```

Si queremos conectarnos mediante SSH, podemos obtener la cofiguración mediante el siguiente comando.

```
vagrant ssh-config
```

Si queremos borrar todas las máquinas creadas, lanzaremos el siguiente comando:

```
vagrant destroy
```

<a name="usage-modules"/>

#### Modules:  (i.e. Ping / Pong)

Lista completa de módulos: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#modules

Vamos a probar que tenemos conexion con todos los _remote hosts_ mediante el módulo ping:

`ansible all -m <module> ` o `ansible all -m <module> -u <user>`

```
ansible all -m ping
ansible all -m ping -u debian
```

![](ansible/imgs/ping.PNG)


<a name="usage-cmd"/>

#### Commands:  (i.e. df-h)
 
Ahora obtendremos el estado/uso de los discos de los distintos remote hosts

```
ansible all -a "df -h"
```

En realidad, aunque no especifiquemos el módulo de ejecución, ansible utilizará el módulo por defecto llamado `command`

![](ansible/imgs/cmd.PNG)


<a name="basic-playbook"/>

#### Playbook básico

Los playbook de ansible permiten ejecutar comandos de forma secuencial. Dichos playbooks siguen la sintaxsis de YAML. Un playbook básico que instale docker en todos los host seria asi:

```
---
- hosts: docker
  become: true
  tasks:
    - name: Install docker dependecies (1/5)
      apt:
        name: "{{item}}"
        state: present
        update_cache: yes
      loop:
        - apt-transport-https
        - ca-certificates
        - curl
        - gnupg-agent
        - software-properties-common

    - name: Add GPG key (2/5)
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Add docker repo (3/5)
      apt_repository:
        repo: deb https://download.docker.com/linux/ubuntu bionic stable
        state: present

    - name: Install docker, docker-cli and conteiner.io (4/5)
      apt:
        name: "{{item}}"
        state: latest
        update_cache: yes
      loop:
        - docker-ce
        - docker-ce-cli
        - containerd.io

    - name: Check if docker is enabled/actibve (5/5)
      service:
        name: docker
        state: started
        enabled: yes
```

Para ejecutar el playbook lanzamos el siguiente comando:

```
ansible-playbook -i inventory -u vagrant install-docker.playbook.yml
```

Ansible se conectará a los host y lanzará los comandos necesarios. Fijemonos en el resultado:

![](ansible/imgs/docker-install.PNG)

![](ansible/imgs/docker-install-result-1.PNG)

Ansible trabaja con el concepto de **idempotencia**, esto es que los comandos que ejecutamos tendran "persistencia". Si lanzamos un playbook que instala un paquete X, y luego lanzamos el mismo u otro playbook que instala el paquete X, la segunda vez que lo lanzamos, no se realizará ningun cambio/instalación puesto que el paquete ya fué instalado la primera vez.

Para ver el concepto de idempotencia, fijemonos en la imagen anterior: El `Play recap`. Nos fiajmos que hay `4 changes`, esto signigfica que ha realizado una tarea que ha realizado cambios en el host remoto.

Si ahora volvemos a ejecutar por `segunda/tercera/cuarta/.../n` vez el playbook, no ralizará nuevos cambios. A esto se le conoce como idempotencia:

```
ansible-playbook -i inventory -u vagrant install-docker.playbook.yml
```

![](ansible/imgs/docker-install-result-2.PNG)

<a name="playbook-vagrant"/>

#### Vagrant provision with playbook (Linux/Mac)

En muchas ocasiones necesitamos disponer de una infrastructura con un software instalado de base. Vagrant permite ejecutar playbooks para aprovisionar las máquinas. Para ello añadimos el siguiente trozo de código al `Vagrantfile`:

```
config.vm.provision "ansible" do |ansible|
    ansible.playbook = "docker.playbook.yml"
  end # end ansible
```

Resultado final del `Vagrantfile`:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

cluster = {
  "master" => { :ip => "192.168.10.100", :cpus => 2, :mem => 1024, :disk=> "10GB" },
  "worker-1" => { :ip => "192.168.10.101", :cpus => 1, :mem => 1024, :disk=> "10GB"},
  "worker-2" => { :ip => "192.168.10.102", :cpus => 1, :mem => 1024, :disk=> "10GB"}
}
Vagrant.configure("2") do |config|

  cluster.each_with_index do |(hostname, info), index|

    config.vm.define hostname do |cfg|

      cfg.vm.provider :virtualbox do |vb, override|
        config.vm.box = "generic/ubuntu2010"
        config.disksize.size = info[:disk]
        override.vm.network :private_network, ip: "#{info[:ip]}", virtualbox__intnet: true
        override.vm.hostname = hostname
        vb.name = hostname  
        vb.memory = info[:mem]
        vb.cpus = info[:cpus]
        
        config.vm.provision "ansible" do |ansible|
          ansible.playbook = "docker.playbook.yml"
        end # end ansible

      end # end provide
      
    end # end config

  end # end cluster
end
```

Si aun no hemos levantado/creado las máquinas, el propio comando `vagrant up` creará y aprovisionará las máquinas. En caso de ya disponder de máquinas creadas y levantadas, podemos aprovisionarlas mediante el comando:

```
vagrant provision
```

<a name="ansible-cfg"/>

#### ansible.cfg

**Nota**: Lista completa de configuraciones: https://docs.ansible.com/ansible/latest/reference_appendices/config.html

Puede llegar a ser tedioso estar especificando constantemente el fichero de inventario a utilizar. Para eso y muchisimas cosas más, Ansible nos proporciona una forma sencilla de configurar la ejecución de comandos. 

**Importante**: El fichero `ansible.cfg` solo tendrá efecto si se ejecutan los comandos ansible desde el mismo directorio que el fichero de configuración.

Veamos un ejemplo del fichero `ansible.cfg`

```
[defaults]
inventory = demo-inventory # path al fichero de inventario por defecto
interpreter_python = /usr/bin/python3 #indicar dónde debe buscar el interprete de python de los hosts remotos
host_key_checking = False #Recomendado SOLO entornos locales: En muchas ocasiones crearemos y destruiremos VMs con las mismas IPs, por ello es aconsejable no comprobar los fingerpints de los hosts

[ssh_connection]
pipelining = True # Reutilizar las conexiones SSH para ejecutar las distintas tareas. Sino, cada tarea debe iniciar una conexion SSH

```

Ahora en vez de ejecutar `ansible -i <inventory-file> all -a "date"` ejecutaremos `ansible all -a "date"` 

<a name="cap2"/>

### Capitulo 2: Inventarios

**Nota**: Lista de variables de inventario: https://docs.ansible.com/ansible/2.7/user_guide/intro_inventory.html#list-of-behavioral-inventory-parameters

Recuperemos uno de los inventarios que definimos anteriormente:

```
[database]
192.168.1.110

[webserver]
192.168.1.111
192.168.1.112
```

Como podriamos ejecutar un playbook para los dos tipos de servidores: `database` y `webserver`. Ansible permite agrupar los distintos tipos de servidores de la siguiente manera.

```
[database]
192.168.1.110

[webserver]
192.168.1.111
192.168.1.112

# Grupo que contien los servidores de tipo database + webservers
[webapp:children]
database
webserver
```

Tambien podemos definir alguna variable para realizar las conexiones SSH como el `usuario` y `private_key`:

```
[database]
192.168.1.110

[webserver]
192.168.1.111
192.168.1.112

# Grupo que contien los servidores de tipo database + webservers
[webapp:children]
database
webserver

# Variables para todos los serviodres webapp (database + webserver)
[webapp:vars]
ansible_user=vagrant
ansible_ssh_private_key_file=/home/vagrant/.ssh/id_rsa
```

Supongamos que tenemos distintas claves y usuarios ssh para conectarnos a cada host, en este caso, la configuración de grupos no nos será de mucha ayuda, en vez, las definiremos a nivel de host.

```
[database]
192.168.1.110 ansible_port=2222 ansible_user=database-manager ansible_ssh_private_key_file=/home/vagrant/.ssh/databasePrivKey

[webserver]
192.168.1.111 ansible_user=web-manager ansible_ssh_private_key_file=/home/vagrant/.ssh/webServerPrivKey1
192.168.1.112 ansible_user=web-manager ansible_ssh_private_key_file=/home/vagrant/.ssh/webServerPrivKey2

# Grupo que contien los servidores de tipo database + webservers
[webapp:children]
database
webserver
```

