package com.myorg;

import java.util.Arrays;
import java.util.Map;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.ec2.AmazonLinuxImage;
import software.amazon.awscdk.services.ec2.ISecurityGroup;
import software.amazon.awscdk.services.ec2.Instance;
import software.amazon.awscdk.services.ec2.InstanceType;
import software.amazon.awscdk.services.ec2.Peer;
import software.amazon.awscdk.services.ec2.Port;
import software.amazon.awscdk.services.ec2.SecurityGroup;
import software.amazon.awscdk.services.ec2.SubnetConfiguration;
import software.amazon.awscdk.services.ec2.SubnetSelection;
import software.amazon.awscdk.services.ec2.SubnetType;
import software.amazon.awscdk.services.ec2.Vpc;

public class DemoStack extends Stack {
	
    public DemoStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public DemoStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        // The code that defines your stack goes here.
        Vpc demoVpc = Vpc.Builder.create(this, "demo-vpc")
        	.cidr("172.35.0.0/16")
        	.subnetConfiguration(
        			Arrays.asList(
	        			new SubnetConfiguration.Builder()
	        			.name("demo-subnet")
	        			.cidrMask(24)
	        			.subnetType(SubnetType.PUBLIC)
	        			.build()       			
					)
        		)
        	.build();
        
        SecurityGroup demoSecurityGroup = SecurityGroup.Builder.create(this, "demo-security-group")
        		.vpc(demoVpc)
        		.allowAllOutbound(false)
        		.build();
        
        demoSecurityGroup.addEgressRule(Peer.anyIpv4(), Port.tcp(22));
        demoSecurityGroup.addEgressRule(Peer.ipv4("172.35.0.0/16"), Port.allTcp());
        
		Instance.Builder.create(this, "MasterInstance")
        	.machineImage(new AmazonLinuxImage())
        	.instanceType(new InstanceType("t2.small"))
        	.keyName("AWS-xkey")
        	.privateIpAddress("172.35.16.11")
        	.vpc(demoVpc)
        	.vpcSubnets(new SubnetSelection.Builder().subnetGroupName("demo-subnet").build())
        	.securityGroup(demoSecurityGroup)
        	.build();
    }
}
