# DevOps Tech things

## Table of contents

- [Infrastructure]()
    - [Tools]()
        - [Ansible](ansible/index.md)
        - [CloudFormation](cloud-formation/index.md)
        - [Dry #TODO]()
    - [Services]()
        - [Docker & dockercompose #TODO]()
        - [Docker Swarm #TODO]()
        - [Kubernetes](k8s/index.md)
    - [Monitoring]()
        - [Prometheus & Grafana](monitoring/prometheus-grafana/index.md)
- [Microservices]()